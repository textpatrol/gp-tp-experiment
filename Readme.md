# Description

This repository contains the experiment source code to compare the performance of Google Perspective with and without Textpatrol as a deobfuscator. The test data is located in the `data/toxic-comments-dataset.xlsx` file.

# Prerequisites

- This source code has been compiled in linux using Go version 1.7.4
- Export the `TP_KEY` environment variable with the textpatrol licence key, e.g. `$ export TP_KEY="MY KEY"`

# Build

`$ go build`

# Run

Run `$ ./gp-tp-experiment $experiment_file_path $output_file_path`. The input file is expected to be an excel file following the structure of `./data/toxic-comments-dataset.xlsx`.
The output file will be a tab delimited file with the results, an example file is in `./data/result_example.txt`.

Run Example:
```
$ TP_KEY="my TP access key token" ./gp-tp-experiment ./data/toxic-comments-dataset.xls restuls.txt
```
